# Todo App with Elchemy

This project is sample for [Elchemy](https://wende.github.io/elchemy/) and [Phoenix](http://phoenixframework.org/).

## Usage

* Install [Elchemy](https://wende.gitbooks.io/elchemy/content/INSTALLATION.html) and [Phoenix](https://hexdocs.pm/phoenix/installation.html).
* Install dependencies with `mix deps.get`
* Install Node.js dependencies with `cd assets && npm install`
* Install Elm dependencies with `cd lib/web/elm && elm packages install`
* Start Phoenix endpoint with `mix phx.server`

To start your Phoenix server:

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
