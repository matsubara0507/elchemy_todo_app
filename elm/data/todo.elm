module Data.Todo exposing (..)

import Dict


type alias Todo =
    { id : String
    , title : String
    , done : Bool
    }


type alias Todos =
    Dict.Dict String Todo
