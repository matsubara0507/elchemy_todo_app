module Models.Todo exposing (..)

import Data.Todo exposing (Todo, Todos)
import Dict
import Elchemy exposing (..)


{- ex
   use GenServer

   def start_link(init \\ %{ todos: %{}, cnt: 0 }), do: GenServer.start_link(__MODULE__, init, name: :todos)

   # GenServer Callback functions
   def init(state), do: {:ok, state}

   def handle_call(:get, _client, state), do: {:reply, state, state}

   def handle_cast({:set, new_state}, _state), do: {:noreply, new_state}

   def gen_(params) do
     %{
       id: params["id"],
       title: params["title"],
       done: params["done"]
     }
   end
-}


type alias State =
    { todos : Todos
    , cnt : Int
    }


type Name
    = Todos


type Action
    = Get
    | Set State


gets : List Todo
gets =
    Dict.values (.todos getState)


add : Todo -> List Todo
add todo =
    let
        { todos, cnt } =
            getState

        newId =
            toString cnt

        newTodo =
            { todo | id = newId }

        state =
            { todos = Dict.insert newId newTodo todos, cnt = cnt + 1 }
    in
    setState state
        |> .todos
        |> Dict.values


update : Todo -> List Todo
update todo =
    let
        { todos, cnt } =
            getState

        state =
            { todos = Dict.update todo.id (Maybe.map <| always todo) todos, cnt = cnt }
    in
    setState state
        |> .todos
        |> Dict.values


remove : String -> List Todo
remove todoId =
    let
        { todos, cnt } =
            getState

        state =
            { todos = Dict.remove todoId todos, cnt = cnt }
    in
    setState state
        |> .todos
        |> Dict.values


gen : params -> Todo
gen =
    ffi "Models.Todo" "gen_"


getState : State
getState =
    call_ Todos Get


setState : State -> State
setState state =
    cast_ Todos (Set state) |> always state


call_ : Name -> Action -> a
call_ =
    ffi "GenServer" "call"


cast_ : Name -> Action -> a
cast_ =
    ffi "GenServer" "cast"
