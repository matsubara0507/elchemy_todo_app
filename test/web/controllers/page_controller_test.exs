defmodule ElchemyTodoAppWeb.PageControllerTest do
  use ElchemyTodoAppWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Todo App by Elchemy"
  end
end
