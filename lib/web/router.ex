defmodule ElchemyTodoAppWeb.Router do
  use ElchemyTodoAppWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  scope "/", ElchemyTodoAppWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", ElchemyTodoAppWeb do
    pipe_through(:api)

    resources("/todos", TodoController, only: [:index, :create, :update, :delete])
  end
end
