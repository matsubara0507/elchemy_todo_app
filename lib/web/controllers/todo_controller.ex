defmodule ElchemyTodoAppWeb.TodoController do
  alias Models.Todo, as: Todo

  use ElchemyTodoAppWeb, :controller

  def index(conn, _params), do: render(conn, "todos.json", %{todos: Todo.gets()})

  def create(conn, params), do: render(conn, "todos.json", %{todos: Todo.add(Todo.gen(params))})

  def update(conn, params),
    do: render(conn, "todos.json", %{todos: Todo.update(Todo.gen(params))})

  def delete(conn, %{"id" => id}), do: render(conn, "todos.json", %{todos: Todo.remove(id)})
end
