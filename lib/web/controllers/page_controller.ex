defmodule ElchemyTodoAppWeb.PageController do
  use ElchemyTodoAppWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
