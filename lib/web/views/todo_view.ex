defmodule ElchemyTodoAppWeb.TodoView do
  use ElchemyTodoAppWeb, :view

  def render("todos.json", %{todos: todos}), do: todos
end
