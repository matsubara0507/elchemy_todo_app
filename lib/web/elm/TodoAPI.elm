module TodoAPI exposing (..)

import Data.Todo exposing (Todo)
import Http
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (..)
import Json.Encode
import String


decodeTodo : Decoder Todo
decodeTodo =
    decode Todo
        |> required "id" string
        |> required "title" string
        |> required "done" bool


encodeTodo : Todo -> Json.Encode.Value
encodeTodo x =
    Json.Encode.object
        [ ( "id", Json.Encode.string x.id )
        , ( "title", Json.Encode.string x.title )
        , ( "done", Json.Encode.bool x.done )
        ]


baseUrl : String
baseUrl =
    "api"


getTodos : Http.Request (List Todo)
getTodos =
    Http.request
        { method =
            "GET"
        , headers =
            []
        , url =
            String.join "/"
                [ baseUrl
                , "todos"
                ]
        , body =
            Http.emptyBody
        , expect =
            Http.expectJson (list decodeTodo)
        , timeout =
            Nothing
        , withCredentials =
            False
        }


postTodos : Todo -> Http.Request (List Todo)
postTodos body =
    Http.request
        { method =
            "POST"
        , headers =
            []
        , url =
            String.join "/"
                [ baseUrl
                , "todos"
                ]
        , body =
            Http.jsonBody (encodeTodo body)
        , expect =
            Http.expectJson (list decodeTodo)
        , timeout =
            Nothing
        , withCredentials =
            False
        }


putTodosById : String -> Todo -> Http.Request (List Todo)
putTodosById capture_id body =
    Http.request
        { method =
            "PUT"
        , headers =
            []
        , url =
            String.join "/"
                [ baseUrl
                , "todos"
                , capture_id |> Http.encodeUri
                ]
        , body =
            Http.jsonBody (encodeTodo body)
        , expect =
            Http.expectJson (list decodeTodo)
        , timeout =
            Nothing
        , withCredentials =
            False
        }


deleteTodosById : String -> Http.Request (List Todo)
deleteTodosById capture_id =
    Http.request
        { method =
            "DELETE"
        , headers =
            []
        , url =
            String.join "/"
                [ baseUrl
                , "todos"
                , capture_id |> Http.encodeUri
                ]
        , body =
            Http.emptyBody
        , expect =
            Http.expectJson (list decodeTodo)
        , timeout =
            Nothing
        , withCredentials =
            False
        }
