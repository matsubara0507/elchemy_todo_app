# Compiled using Elchemy v0.7.4
defmodule Data.Todo do
  use Elchemy

  alias Elchemy.XDict

  @type todo :: %{
          id: String.t(),
          title: String.t(),
          done: boolean
        }

  @type todos :: Dict.dict(String.t(), todo)
end
