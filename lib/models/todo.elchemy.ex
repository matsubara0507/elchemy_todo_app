# Compiled using Elchemy v0.7.4
defmodule Models.Todo do
  use Elchemy

  alias Data.Todo
  alias Elchemy.XDict

  use GenServer

  def start_link(init \\ %{todos: %{}, cnt: 0}),
    do: GenServer.start_link(__MODULE__, init, name: :todos)

  # GenServer Callback functions
  def init(state), do: {:ok, state}

  def handle_call(:get, _client, state), do: {:reply, state, state}

  def handle_cast({:set, new_state}, _state), do: {:noreply, new_state}

  def gen_(params) do
    %{
      id: params["id"],
      title: params["title"],
      done: params["done"]
    }
  end

  @type state :: %{
          todos: Data.Todo.todos(),
          cnt: integer
        }

  @type name :: :todos

  @type action :: :get | {:set, state}

  @spec gets() :: list(Data.Todo.todo())
  def gets() do
    Elchemy.XDict.values((fn a -> a.todos end).(get_state()))
  end

  @spec add(Data.Todo.todo()) :: list(Data.Todo.todo())
  curry(add / 1)

  def add(todo) do
    %{todos: todos, cnt: cnt} = get_state()
    new_id = to_string().(cnt)
    new_todo = %{todo | id: new_id}
    state = %{todos: Elchemy.XDict.insert(new_id, new_todo, todos), cnt: cnt + 1}

    set_state(state)
    |> (fn a -> a.todos end).()
    |> Elchemy.XDict.values().()
  end

  @spec update(Data.Todo.todo()) :: list(Data.Todo.todo())
  curry(update / 1)

  def update(todo) do
    %{todos: todos, cnt: cnt} = get_state()

    state = %{
      todos:
        Elchemy.XDict.update(
          todo.id,
          always().(todo)
          |> Elchemy.XMaybe.map().(),
          todos
        ),
      cnt: cnt
    }

    set_state(state)
    |> (fn a -> a.todos end).()
    |> Elchemy.XDict.values().()
  end

  @spec remove(String.t()) :: list(Data.Todo.todo())
  curry(remove / 1)

  def remove(todo_id) do
    %{todos: todos, cnt: cnt} = get_state()
    state = %{todos: Elchemy.XDict.remove(todo_id, todos), cnt: cnt}

    set_state(state)
    |> (fn a -> a.todos end).()
    |> Elchemy.XDict.values().()
  end

  @spec gen(any) :: Data.Todo.todo()

  curry(gen / 1)
  verify(as: Models.Todo.gen_() / 1)
  def gen(a1), do: Models.Todo.gen_(a1)
  @spec get_state() :: state
  def get_state() do
    call_(:todos, :get)
  end

  @spec set_state(state) :: state
  curry(set_state / 1)

  def set_state(state) do
    cast_(:todos, {:set, state})
    |> always().(state).()
  end

  @spec call_(name, action) :: any

  curry(call_ / 2)
  verify(as: GenServer.call() / 2)
  def call_(a1, a2), do: GenServer.call(a1, a2)
  @spec cast_(name, action) :: any

  curry(cast_ / 2)
  verify(as: GenServer.cast() / 2)
  def cast_(a1, a2), do: GenServer.cast(a1, a2)
end
